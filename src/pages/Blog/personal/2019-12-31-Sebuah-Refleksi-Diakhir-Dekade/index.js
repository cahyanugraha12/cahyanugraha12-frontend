import React from 'react';
import { CardMedia, CardContent, Grid, GridList, GridListTile, GridListTileBar, Link, Typography } from '@material-ui/core';
import withWidth, { isWidthUp } from '@material-ui/core/withWidth';

const SebuahRefleksiDiakhirDekade = (props) => {
  const toggleSpoiler = (e) => {
    e.currentTarget.classList.toggle("off");
  };

  const getGridListCols = () => {
    if (isWidthUp('md', props.width)) {
      return 4;
    }

    if (isWidthUp('sm', props.width)) {
      return 2;
    }

    return 1;
  }

  return (
    <Grid container direction="row" justify="center" spacing={2}>
      <Grid item md={8} xs={10}>
        <Typography variant="h2" component="h1">Sebuah Refleksi Diakhir Dekade</Typography>
        <Typography variant="subtitle1">First published on 31 Desember 2019 by Pande Ketut Cahya Nugraha</Typography>
        <Typography variant="subtitle1">Last edited on 31 December 2019</Typography>
        <Typography variant="subtitle1">Current version: v1</Typography>
      </Grid>
      <Grid item md={8} xs={10}>
        <div>
          <Grid container justify="center">
            <Grid item md={6} xs={10}>
              <CardMedia
                component="img"
                image="https://storage.googleapis.com/pr-newsroom-wp/1/2019/12/Wrapped-Header.jpg"
                title="Kebiasaan akhir tahun, pamer Spotify Wrapped."
              />
            </Grid>
          </Grid>
          <CardContent>
            <Typography variant="subtitle1" align="center" color="textSecondary">
              Kebiasaan akhir tahun, pamer Spotify Wrapped.
            </Typography>
          </CardContent>
        </div>
      </Grid>
      <Grid item md={8} xs={10}>
        <Typography variant="body1" align="justify" paragraph>
          Akhirnya setelah wacana bertahun-tahun (dari Semester 2), saya berhasil bikin satu buah post blog (Essay PemFung doesn't count).
          Beragam alasan yang membuat procras terpatahkan, dari bilang mau bikin sistemnya sendiri (padahal bisa blogspot atau pake gatsby), 
          ga punya domain (padahal bisa di github.io atau netlify), sampe ga punya server (padahal server nganggur dari lama).
          Supaya saya nggak jadi traktir kalian-kalian yang SS story IGku <span className="spoiler" onClick={(e) => toggleSpoiler(e)}>yang sebenernya kubikin niatnya biar *** *** ******</span>, kita mulai saja post blog ini.
        </Typography>
      </Grid>
      <Grid item md={8} xs={10}>
        <Typography variant="h4" component="h2">Semester 4</Typography>
        <Typography variant="body1" align="justify" paragraph>
          Judging dari story-story yang kubuat di semester 4, kebanyakan ceritanya akademis, jadi kalian mending baca blognya Fairuzi sih kalo 
          masalah akademis semester 4 di <Link href="https://fairuzi10.com" target="_blank">fairuzi10.com</Link>. Kalo personal event yang mengesankan sih paling
          <ul>
            <li>Kena tipes sampe bolos seminggu lebih (makanya Skip Listku pas semester 4 bisa sampe 30 kali :|) dan jadi benci air putih sampe sekarang (suer, enek banget minum air 3-5 liter sehari).</li>
            <li>Belajar bahwa bantu orang ga jamin kamu bakal dibantu kembali (assuming orangnya sebenernya mampu bantu kamu, kalo emang ga mampu mah ya gapapa), so hati-hati aja, don't be a yes man.</li>
          </ul>
          Other than that, sepertinya hidupku pas semester 4 ya gitu-gitu aja, akademis akademis panitia organisasi tidur makan nugas repeat. 
        </Typography>
      </Grid>
      <Grid item md={8} xs={10}>
        <Typography variant="h4" component="h2">Magang</Typography>
        <div>
          <Grid container justify="center">
            <Grid item md={6} xs={10}>
              <CardMedia
                component="img"
                image="https://instagram.fsub4-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s750x750/69690313_160211551824776_6751178563055252025_n.jpg?_nc_ht=instagram.fsub4-1.fna.fbcdn.net&_nc_cat=108&_nc_ohc=Dfxp7zD7XR0AX8GE-J7&oh=67760848b95db8adb71239c50dd892e1&oe=5EA42F21"
                title="Hari terakhir magang :)"
              />
            </Grid>
          </Grid>
          <CardContent>
            <Typography variant="subtitle1" align="center" color="textSecondary">
              Hari terakhir magang :)
            </Typography>
          </CardContent>
        </div>
        <Typography variant="body1" align="justify" paragraph>
          *Bulan Januari 2019*<br/> 
          *tinung*<br/>
          *baru bangun, buka hape, liat FB messenger*<br/>
          Ci Nia: Cah, mau magang di Cermati ga ntar summer?<br/>
          Me: *setengah ngantuk* Hmm? bole-bole aja<br/>
          Ci Nia: Oke, send CV ya, trus entar interview<br/>
          Me: *masih setengah ngantuk* okeyyy<br/>
          <br/>
          Me: WAIT A SECOND AKU BELOM ADA PERSIAPAN INTERVIEW APA2<br/>
          <br/>
          Yep, begitulah awal gimana aku bisa masuk Cermati. Literally baru belajar buat interview seminggu sebelumnya. Entah keajaiban apa yang jatuh dari atas, 
          padahal interviewku ga bagus-bagus amat tapi ternyata good enough. And so, aku yang awalnya considering mau tidur aja 3 bulan pas liburan malah udah dapet magang 
          6 bulan sebelumnya.
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          To be honest, aku expect magang ya gitu-gitu aja. Setelah 2 kali magang sebelumnya, ekspektasiku ya bangun, makan, kegencet kereta, ngoding, makan siang, ngoding lagi, kegencet kereta lagi, tidur, repeat.
          But nope, disini aku belajar banyak hal yang sebelumnya berada diluar zona nyaman aku, seperti inisiatif nanya ke client tentang gimana software mereka bekerja, 
          berani <strike>ganggu</strike> tanya-tanya ke kaka-kaka full time yang baik hati dan tidak sombong, konstan berkomunikasi sama PM tentang proyeknya, <strike>menggunakan waktu makan siang untuk pergi jalan-jalan ke Grand Indonesia</strike>, berani speak up kalo ada problem, dst.
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          You might notice that most of that is about soft skill (specifically, communication skill). Karena emang itu sih yang bagi aku paling mengesankan dari magang kali ini.
          Codewise, aku rasa aku lancar-lancar aja (walau ga 100% perfect sih), tapi itu weightnya jauh dibawahnya apa yang aku tulis diatas. 
          Aku ngerasa banget magang di Cermati ini membantu confidence aku, dan itu sangat mengesankan dan berharga buat aku (apalagi aku orangnya diem ayem gaberani ngomong).
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          Sebenernya banyak sih yang sebenernya bisa aku ceritain, tapi masalahnya udh samar-samar dikepala dan kalo mau inget perlu scrolling story dan photos di HP
          , dan fokus dari post ini adalah semester 5, so I guess I will let it lie for now.
        </Typography>
        <div>
          <Grid container justify="center">
            <Grid item md={6} xs={10}>
              <CardMedia
                component="img"
                image="https://instagram.fsub3-1.fna.fbcdn.net/v/t51.2885-15/e35/s1080x1080/67817276_348746406031384_1296403230198268288_n.jpg?_nc_ht=instagram.fsub3-1.fna.fbcdn.net&_nc_cat=104&_nc_ohc=ygCSxEOlPgIAX-Ut0au&oh=760b122f33d7806815ceb94ff800a7dc&oe=5E8FB288"
                title="Immortalized as a Slack Sticker."
              />
            </Grid>
          </Grid>
          <CardContent>
            <Typography variant="subtitle1" align="center" color="textSecondary">
              Immortalized as a Slack Sticker.
            </Typography>
          </CardContent>
        </div>
      </Grid>
      <Grid item md={8} xs={10}>
        <Typography variant="h4" component="h2">Semester 5</Typography>
        <Typography variant="body1" align="justify" paragraph>
          OHOHOHOHO. Seperti yang pernah aku bilang, <code>worst_semester = cahya.current_semester()</code>. 
          Semester 5 bisa jadi merupakan semester paling membingungkan dan menyebalkan dalam hidupku. 
          Cukup menyebalkan sampe aku pernah mikir kayaknya mending SD dan SMP lagi.
          Untungnya berkat teman-temanku di semester 5 ini, bisa kuat melewati semester ini.
        </Typography>

        <Typography variant="h5" component="h3">Mata Kuliah Jahanam</Typography>
        <Typography variant="body1" align="justify" paragraph>
          Terburuk. Titik. Kali pertama dimana aku ga yakin ada satu aja yang A. Kalaupun A, pasti di carry atau ada keajaiban.
          Semester ini aku ambil 3 mata kuliah wajib yang normal (Rekayasa Perangkat Lunak alias RPL, Pemrograman Sistem alias Sysprog, Jaringan Komputer alias Jarkom)
          , 2 mata kuliah peminatan (Perolehan Informasi alias IR, Pemrograman Fungsional alias Pemfung)
          , dan 1 mata kuliah wajib namun nyodok alias sebenernya harusnya diambil di semester 7 yaitu Desain dan Analisis Algoritma alias DAA (ini matkul yang bikin nangis semua mahasiswa CS sedunia sih).
        </Typography>
        <Typography variant="h6" component="h4">IR</Typography>
        <Typography variant="body1" align="justify" paragraph>
          Mulai dari yang paling menyenangkan deh. IR ini diajar oleh dekan Fasilkom tercinta, jadi dari awal masuk aku udh ga berani ga merhatiin dan ga berani bolos.
          Katanya kalo kehadiran ga 75%, langsung 0 UASnya. Serem banget ga sih :"(. Materinya menarik, mulai dari memproses teks, pake bahasa Perl (perl is the future bruh)
          , indeksing dokumen, model-model untuk searching dokumen, mengelompokkan dokumen, dan yang sebenarnya bikin aku ngebet ambil matkul ini, Sentimen Analisis (tapi ternyata ga terlalu banyak dibahas, so sad).
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          Tugasnya, seru sih buatku. Susah juga dijelaskan dimana letak serunya. Mungkin karena aku emang suka mengerjakan sesuatu yang nguli dan eksperimen kali ya.
          Bayangkan kamu mesti mengidentifikasi kata mana yang merupakan tanggal atau lokasi di dokumen. Edge casesnya banyak banget sampe kodeku bisa 500 baris lebih.
          Dan yang paling susah adalah, hasil tiap orang beda-beda, jadi susah konfirmasi apakah hasil kerjaanmu udah bener atau belom.
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          Untungnya UTSnya lumayan gampang, tapi UASnya gila susah banget. 
          UTSnya pendek-pendek dan semua ada di slide, lalu tiba-tiba UASnya butuh mikir keras dan menghafal banyak biar bisa jawab semua.
          Tapi setidaknya aku yakin lah kalo matkul ini lulus :)
        </Typography>
        <div>
          <Grid container justify="center">
            <Grid item md={6} xs={10}>
              <CardMedia
                component="img"
                image="/data/blog/personal/2019-12-31-Sebuah-Refleksi-Diakhir-Dekade/sentimen.png"
                title="Ingin lulus S1 dengan judul skripsi `Menganalisa Perasaan/Sentimen Dirinya terhadap Diriku berdasarkan cuitan di Sosial Media`"
              />
            </Grid>
          </Grid>
          <CardContent>
            <Typography variant="subtitle1" align="center" color="textSecondary">
              Ingin lulus S1 dengan judul skripsi `Menganalisa Perasaan/Sentimen Dirinya terhadap Diriku berdasarkan cuitan di Sosial Media`
            </Typography>
          </CardContent>
        </div>

        <Typography variant="h6" component="h4">RPL, Sysprog, dan Jarkom</Typography>
        <Typography variant="body1" align="justify" paragraph>
          Gaje. Parah. Kombinasi materi yang terlalu banyak dan tidak adanya minat untuk matkul-matkul ini membuatku apatis terhadap matkulnya.
          Aku masuk kelas bener-bener cuma badan doang yang dikelas, pikiran antara di hape atau <span className="spoiler" onClick={(e) => toggleSpoiler(e)}>taulah kemana hehe</span>.
          RPL saking ga tertariknya ga ada materi yang pernah aku meme-in. Sysprog saking bikin trauma aku ga mau mention materi-materinya, bahkan cuma mention Raspberry atau Pi bisa bikin anak Ilkom tertrigger :)
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          Jarkom on the other hand, is very meme-able. Mungkin karena Jarkom erat kaitannya sama internet ya, jadi memenya sangat abundant di dunia maya.
          Mulai dari classic TCP UDP meme, meme perangkat Internet kayak Switch dan Router, sampai meme absurd kayak Hidden Network Problem.
          Sering banget scrolling twitter atau buka story IG anak-anak pacil dan menemukan meme-meme dari slides matkul.
          Sayangnya aku bukan kurator meme jadi tidak bisa memberikan contoh-contoh selain meme-meme Jarkom klasik saja :(.
        </Typography>
        <div>
          <Grid container justify="center">
            <Grid item md={6} xs={10}>
              <CardMedia
                component="img"
                image="https://i.redd.it/duv11av99nm11.png"
                title="One of the most classic Computer Network meme."
              />
            </Grid>
          </Grid>
          <CardContent>
            <Typography variant="subtitle1" align="center" color="textSecondary">
              One of the most classic Computer Network meme.
            </Typography>
          </CardContent>
        </div>

        <Typography variant="h6" component="h4">DAA</Typography>
        <Typography variant="body1" align="justify" paragraph>
          Just don't :(
        </Typography>

        <Typography variant="h6" component="h4">Pemfung</Typography>
        <Typography variant="body1" align="justify" paragraph>
          Matkul yang awalnya sangat antusias aku ikuti dan berakhir menjadi sebuah disappointment. 
          Kalo aku ngerant disini entar ratingnya bisa jadi R so let me summarize it in three sentences.
        </Typography>
        <Typography variant="body1" align="center" paragraph>
          "Yatuhan dosa hamba apa yatuhan. Bentar ini belajar dari mana sih? Ini aku salah masuk ke fakultas Ekonomi ya?"
        </Typography>
        <div>
          <Grid container justify="center">
            <Grid item md={6} xs={10}>
              <CardMedia
                component="img"
                image="https://pbs.twimg.com/media/EL9zHdAVUAIMgJA?format=jpg&name=small"
                title="Udah, terima aja."
              />
            </Grid>
          </Grid>
          <CardContent>
            <Typography variant="subtitle1" align="center" color="textSecondary">
              Udah, terima aja.
            </Typography>
          </CardContent>
        </div>

        <Typography variant="h5" component="h3">Para Penyemangat</Typography>
        <Typography variant="body1" align="justify" paragraph>
          Begitulah tahun 2019 ku. Masih banyak yang bisa aku tulis sebenarnya, tapi kombinasi dari deadliner, lack of writing skills
          , dan <span className="spoiler" onClick={(e) => toggleSpoiler(e)}>belum selesainya sophisticated sistem hierarchy spoiler yang bisa membuatku menampilkan beberapa section ke orang-orang tertentu saja</span> membuatku hanya bisa menulis sampai segini saja.
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          Tidak lupa sebuah apresiasi kepada para penyemangat yang membuatku mampu melewati tahun 2019 ini.<br/>
          Family members isn't included (really, I don't have to write it out for you guys, right?)<br/>
          Not that if you aren't mentioned here you didn't matter, you do, maybe I just forgot, you know how I am :")<br/>
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          <ul>
            <li>Rey: Manusia paling bgst sedunia.</li>
            <li>Nida: Sobat ******* semoga kapal-kapalmu tidak karam lagi.</li>
            <li>Ama: Partner DAA yang tabah menghadapi diriku yang suka sekip dan sabar karena sering aku ganggu dengan curhatanku. And no, ma, aku ganteng :(</li>
            <li>Tasput: Yang aku selalu curhatin dan paling sering aku ganggu. Jangan minecraft melulu, kasian serverku. Eh ga ding silahkan minecraft terus tapi inget bagi hasil ke aku :v</li>
            <li>Faiz: Yang menginspirasi bikin blog ini. Asdos Jarkomdat yuk biar sukses kayak ******.</li>
            <li>Hira: Yang selalu memberikan wejangan bagus tiap aku ganggu, and then say bye karena saking muaknya dengerin gw.</li>
            <li>FwP: Yang telah carry tugas kelompokku dan suka ngirim weird stuff on unholy hours.</li>
            <li>Aab: Semoga kita berdua kuat ab :")</li>
            <li>Miba: Manusia bgst nomor dua, dengan segala advice dan kata-kata kasarnya</li>
            <li>Andre: Manusia bgst nomor tiga, dengan segala ke-absurd-an yang menandingi FwP, tapi bisa tiba-tiba bijak entah kesambet apa.</li>
            <li>Nasya: Yang suka diajak misuhin PMB bareng, mangat magangnya, jangan peluk beruang asli okay? Peluk Ice Bear aja.</li>
            <li>Nico: Pawangnya Andre, jagain Andre di Aussie biar baik-baik saja ya :)</li>
            <li>Will: Yang tabah punya Vice Head kayak aku :)</li>
            <li>Asti: Yang tiap muncul tugas Jarkom gw udh ready buat bales chatnya. Semoga ntar PPL kita berdua baik-baik saja :)</li>
          </ul>
        </Typography>

        <GridList cellHeight={200} cols={getGridListCols()}>
            <GridListTile cols={1}>
              <img 
                src="https://instagram.fsub4-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/80455573_160434645215279_1836429946894751337_n.jpg?_nc_ht=instagram.fsub4-1.fna.fbcdn.net&_nc_cat=111&_nc_ohc=iF8VaNPj-OAAX9MVokM&oh=29942ab587016bd0ad210fad355be450&oe=5E9DAD86"
                alt="Timbul Webdev."
              />
              <GridListTileBar
                title="Timbul Webdev."
              />
            </GridListTile>
            <GridListTile cols={1}>
              <img 
                src="https://instagram.fsub3-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/79222848_294657521488009_126739106537339536_n.jpg?_nc_ht=instagram.fsub3-1.fna.fbcdn.net&_nc_cat=101&_nc_ohc=eTvSV_xPcl0AX_ErQL_&oh=c80015cdb6b268bc9d67fe423fde75a2&oe=5EAD71C3"
                alt="Karaoke galau."
              />
              <GridListTileBar
                title="Karaoke galau."
              />
            </GridListTile>
            <GridListTile cols={1}>
              <img 
                src="https://instagram.fsub3-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/79385111_743753639466194_6178291100172988868_n.jpg?_nc_ht=instagram.fsub3-1.fna.fbcdn.net&_nc_cat=102&_nc_ohc=YPln06aetncAX-jwaI0&oh=d4f302836ae6fa42a6e09c923d1fe4f1&oe=5EAAEF24"
                alt="TOKI UI Gathering 2019."
              />
              <GridListTileBar
                title="TOKI UI Gathering 2019."
              />
            </GridListTile>
            <GridListTile cols={1}>
              <img 
                src="https://instagram.fsub4-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/75458071_614675302601926_5595086588771766863_n.jpg?_nc_ht=instagram.fsub4-1.fna.fbcdn.net&_nc_cat=110&_nc_ohc=6oh1aoBkjowAX_ygP35&oh=f20f0435dadc3243c7315e1ffd8673cf&oe=5E9E57FB"
                alt="Ristek Gathering(?)."
              />
              <GridListTileBar
                title="Ristek Gathering(?)."
              />
            </GridListTile>
            <GridListTile cols={1}>
              <img 
                src="https://instagram.fsub3-2.fna.fbcdn.net/v/t51.2885-15/e35/73030094_239868333658255_4833394373790101618_n.jpg?_nc_ht=instagram.fsub3-2.fna.fbcdn.net&_nc_cat=107&_nc_ohc=YqVf5YDGhgUAX-0NsBN&oh=1124590ceaeb159a4bfbee71610c824b&oe=5E915C56"
                alt="Hari terakhir magang."
              />
              <GridListTileBar
                title="Hari terakhir magang."
              />
            </GridListTile>
            <GridListTile cols={1}>
              <img 
                src="https://instagram.fsub4-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/80790031_601252830675500_3494106987499178579_n.jpg?_nc_ht=instagram.fsub4-1.fna.fbcdn.net&_nc_cat=108&_nc_ohc=40Bu92LtYnwAX9tEe3X&oh=6b534ee23ea9f604b613b57efedd4daf&oe=5EB22907"
                alt="Closing Competition Pekan Ristek."
              />
              <GridListTileBar
                title="Closing Competition Pekan Ristek."
              />
            </GridListTile>
            <GridListTile cols={1}>
              <img 
                src="https://instagram.fsub4-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/77026078_166762031100635_335816268676112916_n.jpg?_nc_ht=instagram.fsub4-1.fna.fbcdn.net&_nc_cat=111&_nc_ohc=qtMMnN1_MCMAX8L-Dxz&oh=85811f035bc84a2675c9a087f8ec904d&oe=5EA01C09"
                alt="Dufan 17 Agustus 2019."
              />
              <GridListTileBar
                title="Dufan 17 Agustus 2019."
              />
            </GridListTile>
            <GridListTile cols={1}>
              <img 
                src="https://instagram.fsub3-1.fna.fbcdn.net/v/t51.2885-15/sh0.08/e35/s640x640/73282824_158459052055383_2550175764234364597_n.jpg?_nc_ht=instagram.fsub3-1.fna.fbcdn.net&_nc_cat=103&_nc_ohc=jekwDt0KB6IAX9c7-mS&oh=55744e16b00e7b72df7b2cfec99235ec&oe=5E9871CE"
                alt="Ngoding bareng di kantor Gojek."
              />
              <GridListTileBar
                title="Ngoding bareng di kantor Gojek."
              />
            </GridListTile>
        </GridList>
      </Grid>
      <Grid item md={8} xs={10}>
        <div className="spoiler" onClick={(e) => toggleSpoiler(e)}>
          <Typography variant="h5" component="h3">Hidden Section</Typography>
          <Typography variant="body1" align="justify" paragraph>
            Sayangnya gw deadliner parah. Padahal gw ada 3 topik lain yang mau kutulis dengan my sophisticated selective spoiler system.
            Ya, tau lah topiknya apa, gampang banget ga sih ditebak. Oh, and no, ini tulisan dibuat deadliner dari jam 1 WITA sampe jam 5 WITA
            , jadi yes I admit sangat buruk. Maybe ntar gw revisi, but knowing me pasti procras so don't count on it :")
          </Typography>
        </div>
      </Grid>
    </Grid>
  );
};

export default withWidth()(SebuahRefleksiDiakhirDekade);