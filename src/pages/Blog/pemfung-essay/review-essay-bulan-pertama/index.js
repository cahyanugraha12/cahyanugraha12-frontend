import React from 'react';
import { CardMedia, CardContent, Grid, Link, Typography } from '@material-ui/core';

const ReviewEssayBulanPertama = (props) => {
  return (
    <Grid container justify="center">
      <Grid item md={8}>
        <Typography variant="h2">Review Essay Bulan Pertama</Typography>
        <Typography variant="subtitle1">Pande Ketut Cahya Nugraha | 1706028663 | PemFung - A</Typography>
        <Typography variant="subtitle2">Pande Ketut Cahya Nugraha</Typography>
        <div>
          <Grid container justify="center">
            <Grid item md="6">
              <CardMedia
                component="img"
                image="https://www.haskell.org/img/haskell-logo.svg"
                title="Logo Haskell, salah satu bahasa pemrograman fungsional yang terkenal."
              />
            </Grid>
          </Grid>
          <CardContent>
            <Typography variant="subtitle1" align="center" color="textSecondary">
              Logo Haskell, salah satu bahasa pemrograman fungsional yang terkenal.
            </Typography>
          </CardContent>
        </div>
        <Typography variant="body1" align="justify" paragraph>
          Pada semester ini, saya mengambil mata kuliah Pemrograman Fungsional. 
          Alasannya adalah karena saya ingin mencoba cara/style pemrograman yang berbeda dengan 
          Procedural Programming maupun Object Oriented Programming. 
          Selain itu, selama magang saat liburan saya juga sempat mencoba React Framework, 
          dimana saya melakukan coding dengan beberapa hal yang disebut-sebut merupakan bagian 
          dari pemrograman fungsional, seperti Higher Order Function dan Pure Function.
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          Kesan pertama saya terhadap Pemrograman Fungsional adalah sintaksnya yang terlihat sulit sekali. Saat saya melihat sintaks Haskell di slide yang ditampilkan di kelas, saya langsung mengalami PTSD karena langsung teringat akan project scoreboard CPC Compfest tahun lalu yang dibuat dengan bahasa Elm. Pada saat itu saya tidak paham sama sekali dengan apa maksud kode dalam project tersebut sehingga ketika saya harus melakukan suatu perubahan, saya mengetikkan kode dengan buta sambil berharap project tetap bisa berjalan setelah saya ubah. Namun, saya tetap bersikeras akan mengambil kelas ini karena saya yakin saya pasti bisa belajar dan paradigma pemrograman fungsional akan berguna bagi saya nantinya.
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          Setelah perkenalan singkat dengan Haskell dan Pemrograman Fungsional, di kelas mulai diajarkan tentang konsep-konsep dalam Pemrograman Fungsional, seperti pattern matching, dimana kita dapat mendefinisikan pola-pola untuk sebuah fungsi, sehingga nanti argumen atau parameter yang kita gunakan untuk memanggil fungsi tersebut akan dicocokkan dengan salah satu pola dalam fungsi, dimana masing-masing pola mempunyai instruksinya sendiri-sendiri (sepemahaman saya). Seperti yang dicontohkan di learnyouahaskell, kita bisa membuat sebuah fungsi yang mengambil sebuah integer, dengan pola bila integer tersebut bernilai 7 maka fungsi akan mengembalikan string “Lucky Number” dan bila integer tersebut tidak bernilai 7 maka fungsi akan mengembalikan string “Not Lucky”. Menurut saya ini serupa dengan if else pada pemrograman prosedural, namun lebih singkat. Selain itu, pattern matching bisa digunakan untuk mendefinisikan fungsi rekursif.
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          Selanjutnya kami belajar mengenai polymorphic function dan higher-order function. Polymorphic function adalah fungsi yang dapat menerima argumen dengan tipe apapun. Contohnya adalah fungsi map, yang bisa digunakan untuk memetakan kumpulan data dengan tipe yang sama ke kumpulan data lain dengan suatu fungsi. Fungsi map dapat menerima fungsi apapun sebagai argumennya, dan sebuah kumpulan data yang tipenya dapat diterima oleh fungsi yang dimasukkan ke map sebagai argumen.
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          Higher-order function bisa dikatakan sebagai komposisi fungsi dalam matematika. Higher-order function dapat mengambil fungsi sebagai argumen atau parameter, serta dapat mengembalikan fungsi. Pada latihan yang diberikan di SCeLE, fungsi iter merupakan sebuah higher-order function, karena dapat menerima argumen berupa sebuah fungsi yang akan dijalankan terhadap data yang diberikan sebanyak n kali. Fungsi map juga bisa dikatakan sebagai suatu higher-order function karena menerima argumen berupa fungsi.
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          Selama mengerjakan latihan, kesulitannya kebanyakan berasal dari sintaks atau cara berpikir secara fungsional itu sendiri. Mungkin karena sebagai mahasiswa Fasilkom saya sudah lebih sering berpikir secara prosedural sehingga cukup sulit untuk mengubah cara berpikir menjadi fungsional. Contohnya pada soal fungsi length, saya cukup lama untuk mendapatkan solusi yang elegan dalam bentuk fungsional, karena insting pertama saya adalah menggunakan loop while untuk mendapatkan banyak elemen yang ada di dalam data. Contoh lainnya adalah kesulitan pada soal fungsi iter dalam mendeklarasikan higher-order function, karena saya sendiri belum terlalu paham dengan sintaks Haskell. Namun, saya tetap akan mendalami pemrograman fungsional untuk menambah wawasan sehingga kedepannya bila terdapat suatu masalah saya bisa memikirkan solusi dari berbagai sudut dan tidak terpaku pada paradigma OOP atau prosedural saja.
        </Typography>
        <Typography variant="body1" align="justify" paragraph>
          Hasil latihan saya dapat dilihat di <Link key={"Latihan"} href={"https://docs.google.com/document/d/1QpCDv59mR1kguqlOVeKvOIRaq8rKqT9Dsy3Vjw-H8Ww/edit?usp=sharing"}>sini</Link>.
        </Typography>
        <Typography variant="subtitle1" align="justify">Dipublikasikan pada tanggal 02 Oktober 2019</Typography>
      </Grid>
    </Grid>
  );
};

export default ReviewEssayBulanPertama;