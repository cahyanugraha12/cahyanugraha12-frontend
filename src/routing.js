import React from 'react';
import { Route, Switch, BrowserRouter as Router } from 'react-router-dom'

import Homepage from 'pages/Homepage';
import ReviewEssayBulanPertama from 'pages/Blog/pemfung-essay/review-essay-bulan-pertama';
import SebuahRefleksiDiakhirDekade from 'pages/Blog/personal/2019-12-31-Sebuah-Refleksi-Diakhir-Dekade';

const Routing = (props) => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={Homepage} />
        <Route exact path="/blog/pemfung-essay/review-essay-bulan-pertama" component={ReviewEssayBulanPertama} />
        <Route exact path="/blog/personal/sebuah-refleksi-diakhir-dekade" component={SebuahRefleksiDiakhirDekade} />
      </Switch>
    </Router>
  );
};

export default Routing;