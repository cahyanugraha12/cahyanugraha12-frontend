import React from 'react';
import ReactDOM from 'react-dom';

import CssBaseline from '@material-ui/core/CssBaseline';
import { createMuiTheme } from '@material-ui/core/styles';
import { ThemeProvider } from '@material-ui/styles';

import * as serviceWorker from './serviceWorker';

import Routing from 'routing';
import Navbar from 'components/Navbar';

import 'static/stylesheet/index.css';

const theme = createMuiTheme({
  typography: {
    fontFamily: 'Gilroy Medium',
  }
});

const App = (
  <ThemeProvider theme={theme}>
    <CssBaseline />
    <Navbar />
    <Routing />
  </ThemeProvider>
)

ReactDOM.render(App, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
