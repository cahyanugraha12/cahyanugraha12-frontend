import React from 'react';
import { AppBar, Grid, Link, Toolbar, Typography } from '@material-ui/core';

const Navbar = (props) => {
  return (
    <AppBar position="sticky">
      <Toolbar>
        <Grid container direction="row" alignItems="center" spacing={2}>
          <Grid item>
            <Link key={"Home"} href={"/"} color="inherit">
              <Typography variant="h6">Cahya's Website</Typography>
            </Link>
          </Grid>
          <Grid item>
            <Link key={"Blog"} href={"/blog"} color="inherit">
              <Typography variant="h6">Blog</Typography>
            </Link>
          </Grid>
        </Grid>
      </Toolbar>
    </AppBar>
  )
};

export default Navbar;
